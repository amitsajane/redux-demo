/* eslint-disable prettier/prettier */
// /* eslint-disable quotes */
// /* eslint-disable react/self-closing-comp */
// /* eslint-disable prettier/prettier */


import React, {Component} from 'react';
import {StatusBar, View,Text} from 'react-native';
// import DrawerNavigator from './src/routes/DrawerNavigator';
import RootStackScreen from './src/routes/RootStackScreen'
import {Provider} from 'react-redux';
import Store from './src/store';



const App = () => {
  return (
     <Provider store={Store}>
    <View style={{flex:1}}>
    <RootStackScreen />
    </View>
     </Provider> 
  );
}

 export default App;