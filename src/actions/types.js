/* eslint-disable prettier/prettier */
export const ADD_USER = 'ADD_USER'
export const OPEN_MODAL = 'OPEN_MODAL'
export const ADD_ITEM = 'ADD_ITEM'
export const REMOVE_ITEM ='REMOVE_ITEM'
export const SELECT_ITEM ='SELECT_ITEM'
