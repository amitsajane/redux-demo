/* eslint-disable comma-dangle */

import {ADD_USER} from './types';

export const addUser = (userFirstName) => {
  return {
    type: ADD_USER,
    payload: userFirstName,
  };
}

export const addUserLastName = (userLastName) => {
  return {
    type: ADD_USER,
    payload: userLastName,
  };
}

export const addPhoneNumber = (userPhoneNumber) => {
  return {
    type: ADD_USER,
    payload: userPhoneNumber,
  };
}