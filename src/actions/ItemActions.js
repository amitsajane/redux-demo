/* eslint-disable keyword-spacing */
/* eslint-disable prettier/prettier */
import {ADD_ITEM} from './types';
import {SELECT_ITEM} from './types';
import {REMOVE_ITEM} from './types'


  
export const addItem = (item) => {
  return{
  type: ADD_ITEM,
  payload: item
};
}

export const selectItem = (id) =>{
  return{
  type:SELECT_ITEM,
  payload:id,
  }
}

export const removeItem = id => ({
  type: REMOVE_ITEM,
  payload: id
})