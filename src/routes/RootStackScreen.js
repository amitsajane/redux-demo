/* eslint-disable eol-last */
/* eslint-disable prettier/prettier */
import React, {Component} from 'react';

import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import SplashScreen from '../screens/SplashScreen';
import SignInScreen from '../screens/SignInScreen';
import SignUpScreen from '../screens/SignUpScreen';
import HomeScreen from '../screens/HomeScreen';
import ImagesScreen from '../screens/ImagesScreen';
import ViewPhotos from '../screens/ViewPhotos'
import SelectImage from '../screens/SelectImage';
const RootStack = createStackNavigator();

const RootStackScreen = ({navigation}) => (
    <NavigationContainer>
    <RootStack.Navigator headerMode='none'>

    <RootStack.Screen name="SignInScreen" component={SignInScreen}/>
    <RootStack.Screen name="SelectImage" component={SelectImage}/>
    <RootStack.Screen name="ImagesScreen" component={ImagesScreen}/>
    <RootStack.Screen name="ViewPhotos" component={ViewPhotos}/>

    {/* <RootStack.Screen name="SelectImage" component={SelectImage}/> */}
    {/* <RootStack.Screen name="SignInScreen" component={SignInScreen}/> */}
    <RootStack.Screen name="Home" component={HomeScreen}/>
    <RootStack.Screen name="SignUpScreen" component={SignUpScreen}/>
        <RootStack.Screen name="SplashScreen" component={SplashScreen}/>



    </RootStack.Navigator>
    </NavigationContainer>
);

export default RootStackScreen;