/* eslint-disable no-undef */
/* eslint-disable react/self-closing-comp */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable no-trailing-spaces */
/* eslint-disable prettier/prettier */

import React, { useState, } from 'react';
import { StyleSheet, TouchableOpacity, Text, TouchableHighlight, View, Button, FlatList } from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons';
import { useSelector, useDispatch } from 'react-redux'
import { addItem } from '../actions/ItemActions'
import { removeItem } from '../actions/ItemActions'


function ListView() {
  const listItems = useSelector(state => state.itemList)
  const [selectedItem, setSelectedItem] = useState(null)



  const selectItem = () => {
    console.log(listItems)
    // const index = listItems.findIndex(
    //   item => data.item.id === item.id
      
    // );

    // console.log('index',index)
    // setSelectedItem(listItems[1].id)
    // console.log('ide',index)

  //  alert('afaf')
  };

  //  console.log(listItems)

  const dispatch = useDispatch()

  return (

    <View
      style={{
        flex: 1,
      }}>


      <FlatList
        data={listItems}
        keyExtractor={item => item.id.toString()}
        renderItem={({ item, index }) => (
          <View style={{}}>

            <TouchableOpacity
              onPress={() => selectItem()}
            >
              <View style={ selectedItem === item.index ? { backgroundColor: '#FFF',
              padding: 10,
              marginTop: 5,
            }
              :
              { 
              backgroundColor: 'red',
              padding: 10,
              marginTop: 5,}}>
              <Text style={styles.itemTitle} numberOfLines={1}>
                {item.name}
              </Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => dispatch(removeItem(item.id))}
              style={styles.button}>

              <Ionicons name='close' color='white' size={20} />
            </TouchableOpacity>
          </View>
        )}
      />

    </View>
  )
}




function SignInScreen({ navigation }) {

  return (

    <View style={styles.container}>
      <ListView />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },

  closeButtonContainer: {
    width: '100%',
    padding: 15,

  },
  listItemContainer: {
    backgroundColor: '#FFF',
    padding: 10,
    marginTop: 5,

  },
  itemTitle: {
    fontSize: 16,
  },

  closeButton: {
    backgroundColor: '#d3d3d3',
    borderRadius: 20,
    width: 40,
    height: 40,
    top: 10,
    alignItems: 'center',
    justifyContent: 'center'
  },
  modalContainer: {
    // backgroundColor:'red',
    justifyContent: 'center',
    alignItems: 'center',

  },
  button: {
    height: 25,
    width: 25,
    borderRadius: 12.5,
    backgroundColor: '#ff333390',
    padding: 5,
    alignItems: 'center',
    justifyContent: 'center',
    position: "absolute",
    top: 10,
    right: 10,

  },

  selected: { backgroundColor: "red" },
  list: { backgroundColor: '#ff00ff' }

})

export default SignInScreen