/* eslint-disable no-return-assign */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
/* eslint-disable no-unused-vars */

import React from 'react';
import { StyleSheet, View, ActivityIndicator, FlatList, Text, TouchableOpacity, Image, TextInput } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { removeItem } from '../actions/ItemActions';
// import {connect} from 'react-redux';



export default class SignUpScreen extends React.Component {

  constructor(props) {
    super(props);
    this.state = {

      dataSource: [
        { id: 1, name: 'aaaaa' },
        { id: 2, name: 'bbbbb' },
        { id: 3, name: 'ccccc' },
        { id: 4, name: 'ddddd' },
        { id: 5, name: 'eeeee' },
        { id: 6, name: 'fffff' },
        { id: 7, name: 'ggggg' },
        { id: 8, name: 'hhhhh' },
        { id: 9, name: 'iiiii' },
        { id: 10, name: 'jjjjj' },
    ],
    };

  }


  FlatListItemSeparator = () => <View style={styles.line} />;

  selectItem = data => {
    data.item.isSelect = !data.item.isSelect;
    data.item.selectedClass = data.item.isSelect ?
      styles.selected : styles.list;
    let index = this.state.dataSource.findIndex(
      item => data.item.id === item.id

    );
    console.log('index',index);
    this.state.dataSource[index] = data.item;
    this.setState({
      dataSource: this.state.dataSource,
    });
    console.log('dataSource',this.state.dataSource)
  };
  deleteItemById = id => {
    const filteredData = this.state.dataSource.filter(item => item.id !== id);
    this.setState({ dataSource: filteredData });
  }
  renderItem = data =>

    <TouchableOpacity
      style={[styles.list, data.item.selectedClass]}
      onPress={() => this.selectItem(data)}
    >
      <Text style={{color:'#000'}}>  {data.item.name} </Text>
      {data.item.selectedClass = data.item.isSelect ?

      <TouchableOpacity
              onPress={() => this.deleteItemById(data.item.id)}
              style={styles.button}>

              <Ionicons name="close" color="white" size={16} />
            </TouchableOpacity> :
            null}
    </TouchableOpacity>


  render() {
    const itemNumber = this.state.dataSource.filter(item => item.isSelect).length;

    if (this.state.loading) {
      return (
        <View style={styles.loader}>
          <ActivityIndicator size="large" color="purple" />
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <Text style={{fontSize:18,textAlign:'center',backgroundColor:'red',color:'#FFF',padding:6,borderRadius:10}}>FlatList Native</Text>

        <FlatList
          data={this.state.dataSource}
          ItemSeparatorComponent={this.FlatListItemSeparator}
          renderItem={item => this.renderItem(item)}
          keyExtractor={item => item.id.toString()}
          extraData={this.state}
        />


      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    margin: 7,
    backgroundColor: '#fff',
    //   paddingVertical: 50,
    position: 'relative',
  },
  numberBox: {
    position: 'absolute',
    // bottom: 25,
    top: 1,
    width: 50,
    height: 50,
    borderRadius: 25,
    left: 250,
    zIndex: 3,
    backgroundColor: '#e3e3e3',
    justifyContent: 'center',
    alignItems: 'center',
  },
  TextInputStyleClass: {
    margin:10,
    textAlign: 'center',
    height: 40,
    borderWidth: 1,
    borderColor: '#009688',
    borderRadius: 7,
    backgroundColor: '#FFFFFF',

  },
  number: { fontSize: 14, color: '#000' },
  selected: { backgroundColor: '#FA7B5F' },
  list:{
    marginTop:5,
    backgroundColor:'#fff',
    padding:10,

  },

  button: {
    height: 24,
    width: 24,
    borderRadius: 12,
    backgroundColor: 'green',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    top: 10,
    right: 10,

  },
});
