/* eslint-disable prettier/prettier */
import React, { Component } from 'react';
import { Alert, Image, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import ImagePicker from 'react-native-image-crop-picker';
import Icon from 'react-native-vector-icons/MaterialIcons';


const styles = StyleSheet.create({
    container: {
        flex: 1,
        // justifyContent: 'center',
        // alignItems: 'center',
        // backgroundColor:'red'
    },
    button: {
        backgroundColor: 'red',
        borderRadius: 5,
        padding: 10,
        marginBottom: 10,
    },
    text: {
        color: 'white',
        fontSize: 20,
        textAlign: 'center',
    },
});

export default class SelectImage extends Component {
    constructor() {
        super();
        this.state = {
            image: null,
            images: [],
        };
    }



    pickMultiple() {
        ImagePicker.openPicker({
            multiple: true,
            waitAnimationEnd: false,
            sortOrder: 'desc',
            includeExif: true,
            forceJpg: true,
        })
            .then((images) => {
                this.setState({
                    image: null,
                    images: images.map((i) => {
                        console.log('received image', i);
                        return {
                            uri: i.path,
                            width: i.width,
                            height: i.height,
                            mime: i.mime,
                        };
                    }),
                });
            })
            .catch((e) => alert(e));
    }

    removeImage(index) {
        const list = this.state.images
        list.splice(index, 1);
        this.setState({ images: list });
        console.log('gsgsgsgsg',this.state.images)
    }

    render() {
        return (
            <View style={styles.container}>
                <ScrollView style={{}}>
                    <View style={{ flexDirection: 'row', flexWrap: "wrap", }}>
                        {this.state.images
                            ? this.state.images.map((image) => (
                                <View   key={image.uri} style={{}}>
                                    <Image
                                        key={image}
                                        style={{ width: 150, height: 150, margin: 10, }}
                                        source={image}
                                    />
                                    <TouchableOpacity
                                        onPress={() => this.removeImage(image)}
                                        style={{ position: 'absolute', bottom: 10, right: 10, height: 25, width: 25, alignItems: "center", justifyContent: "center" }}>

                                        <Icon name="delete-outline" color="white" size={16} />
                                    </TouchableOpacity>
                                </View>
                            ))
                            : null}
                    </View>

                </ScrollView>


                <TouchableOpacity
                    onPress={this.pickMultiple.bind(this)}
                    style={styles.button}
                >
                    <Text style={styles.text}>Select Image</Text>
                </TouchableOpacity>



            </View>
        );
    }
}