/* eslint-disable no-unused-vars */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable no-trailing-spaces */
/* eslint-disable prettier/prettier */
import React, { useState, useEffect } from 'react';
import { StyleSheet, TouchableOpacity, Text, View, Button,TextInput, Alert } from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons';
import {  useDispatch } from 'react-redux';
import {addItem} from '../actions/ItemActions';



function HomeScreen({ navigation }) {


const[name,setName]=useState('')
const dispatch = useDispatch()

useEffect(() => {
  // setValue('')
},)


const onSaveNote = name => {
  dispatch(addItem(name))
  setName('')
  navigation.navigate('SignInScreen')
}

  return (

    <View style={styles.container}>
      <View style={styles.modalContainer}>

        <TextInput
          style={{
            // height: 50,
            width: '100%',
            padding: 10,
            borderColor: 'gray',
            borderWidth: 1,
            borderRadius:5,
            fontSize:18,
          }}
          placeholder={"Enter Item"}
          numberOfLines={1}
          onChangeText={name => setName(name)}
          value={name}
          clearButtonMode='while-editing'
        />
      </View>

      <View style={styles.closeButtonContainer}>
        <Button
          title="Add Item"
          onPress={() => onSaveNote()}
        />
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {

    flex: 1
  },
 
  closeButtonContainer: {
    width: '100%', 
    padding: 15,

  },
  closeButton: {
    backgroundColor: '#d3d3d3',
    borderRadius: 20,
    width: 40,
    height: 40,
    top: 10,
    alignItems: 'center',
    justifyContent: 'center'
  },
  modalContainer: {
    padding:15,
    justifyContent: 'center',
    alignItems: 'center',

  }
})

export default HomeScreen