// /* eslint-disable comma-dangle */
// /* eslint-disable eol-last */
// /* eslint-disable semi */
// /* eslint-disable prettier/prettier */ 
// import { createStore, combineReducers } from 'redux';
// import userReducer from './reducers/reducer';

// const rootReducer = combineReducers({
//   users: userReducer
// });

// const configureStore = () => {
//   return createStore(rootReducer);
// }

// export default configureStore;



import { createStore } from 'redux'
import rootReducer from './reducers/reducer'

const store = createStore(rootReducer)
export default store