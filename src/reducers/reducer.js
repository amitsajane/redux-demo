/* eslint-disable no-trailing-spaces */
/* eslint-disable eol-last */
/* eslint-disable semi */
/* eslint-disable prettier/prettier */
import { ADD_ITEM, SELECT_ITEM } from '../actions/types';
import { REMOVE_ITEM } from '../actions/types'


const initialState = {
    // name:'',

    itemList: [
        { id: 1, name: 'aaaaa' },
        { id: 2, name: 'bbbbb' },
        { id: 3, name: 'ccccc' },
        { id: 4, name: 'ddddd' },
        { id: 5, name: 'eeeee' },
        { id: 6, name: 'fffff' },
        { id: 7, name: 'ggggg' },
        { id: 8, name: 'hhhhh' },
        { id: 9, name: 'iiiii' },
        { id: 10, name: 'jjjjj' },
    ]

}

const rootReducer = (state = initialState, action) => {

    switch (action.type) {
        case ADD_ITEM:
            console.log('name is', action)
            return {

                ...state,
                itemList: state.itemList.concat({
                    id: Math.random(),
                    name: action.payload
                })
            }

        case REMOVE_ITEM:
            return {
                ...state,
                itemList: state.itemList.filter(item => item.id !== action.payload)

            }
            case SELECT_ITEM:{
                return{
                    ...state,
                    
                }
            }

        default:
            return state
    }

}
export default rootReducer